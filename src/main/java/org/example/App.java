package org.example;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    static int argumentsLimit = 10;
    public static final Logger logger = LogManager.getLogger(App.class);

    /**
     * This small method takes up to 10 integers as arguments, sort them and prints into standard output
     *
     * @param args Integers
     * @throws IllegalArgumentException if pass invalid integer
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        logger.log(Level.INFO, "Start main method with args:".concat(Arrays.toString(args)));

        List<Integer> userIntegers = new ArrayList<>();
        int numberOfArgument = 0;

        for (String arg : args) {
            numberOfArgument++;

            if (arg.equals("") || numberOfArgument > argumentsLimit)
                break;

            try {
                userIntegers.add(Integer.parseInt(arg));
            } catch (NumberFormatException e) {
                IllegalArgumentException iae = new IllegalArgumentException("All arguments must be valid integers", e);
                logger.error(iae);
                throw iae;
            }
        }

        sortList(userIntegers);

        String resultOfExecution = userIntegers.toString();
        System.out.println(resultOfExecution);

        String takenTime = String.valueOf(System.currentTimeMillis() - startTime);
        logger.log(Level.INFO, "Result of execution: ".concat(resultOfExecution));
        logger.log(Level.INFO, "Finished execution by ".concat(takenTime).concat(" ms"));
    }


    private static void sortList(List<Integer> userIntegers) {
        long startTime = System.currentTimeMillis();
        logger.log(Level.INFO, "Start sorting...");

        userIntegers.sort((a, b) -> {
            if (a > b) {
                return 1;
            } else if (a < b) {
                return -1;
            }
            return 0;
        });

        String takenTime = String.valueOf(System.currentTimeMillis() - startTime);
        logger.log(Level.INFO, "Sorting finished by ".concat(takenTime).concat(" ms"));
    }
}
